# Go Queue

Common queue abstraction library for [Go](http://golang.org) programs. Currently supports the
[AWS SQS](http://aws.amazon.com/sqs) and [Bitly nsq](http://bitly.github.io/nsq/) queues.

## Publisher

A Publisher is an extremely simple interface to sending messages to a queue.

## Subscriber

A Subscriber is an extremely simple interface to pulling messages from a queue (AWS SQS-style).

## Handler

A Handler is an extremely simple interface to receiving pushed messages from a queue (nsq-style).