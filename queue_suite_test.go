// (c) 2014 Intellimatics LLC. All Rights Reserved.
package queue_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
)

func TestNsq(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "nsq Test Suite")
}
