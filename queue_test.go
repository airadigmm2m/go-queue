// (c) 2014 Intellimatics LLC. All Rights Reserved.
package queue_test

import (

	//. "bitbucket.org/airadigmm2m/sms/nsq"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("nsq", func() {

	BeforeEach(func() {
		// Nothing to do yet
	})

	Describe("Incoming SMS POST", func() {
		Context("with valid SMS", func() {
			It("should return a message", func() {
				Ω("a").Should(Equal("a"))
			})
		})
		Context("with invalid SMS", func() {
			It("should return an error", func() {
				Ω("b").Should(Equal("b"))
			})
		})
	})
})
