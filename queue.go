// (c) 2014 Intellimatics LLC. All Rights Reserved.
package queue

import (
	"strconv"
	"time"

	"github.com/crowdmob/goamz/aws"
	"github.com/crowdmob/goamz/sqs"
)

// ----------------------------------------------------------
// Publisher
// ----------------------------------------------------------
type Publisher interface {
	Publish(message string)
}

type QueuePublisher struct {
	Topic string
}

func NewQueuePublisher(topic string) *QueuePublisher {
	return &QueuePublisher{Topic: topic}
}

func (q *QueuePublisher) Publish(message string) {
}

type MockPublisher struct {
	Messages []string
}

func (p *MockPublisher) Publish(message string) {
	p.Messages = append(p.Messages, message)
}

// ----------------------------------------------------------
// Message
// ----------------------------------------------------------
type Message struct {
	Id      string // A unique ID for the message
	Body    string // The message body - text/plain
	Receipt string // Used to delete the message from a queue
}

// ----------------------------------------------------------
// Subscriber
// ----------------------------------------------------------
type Subscriber interface {
	// Receive message(s) from the queue.
	//
	// max        - the maximum number of messages to receive (limited to 10)
	// visibility - the time messages are hidden from other subscribers
	// wait       - the maximum time to wait before returning without any messages.
	//              if there are messages available, ReceiveMessage returns immediately.
	ReceiveMessage(max int, visibility, wait time.Duration) (messages []Message, err error)
	// Delete message(s) from the queue
	DeleteMessage(message *Message) error
}

// Queue subscribers pull directly from the AWS SQS API
type QueueSubscriber struct {
	Topic string     // Name of the queue topic
	Queue *sqs.Queue // Actual AWS queue
}

func NewQueueSubscriber(topic string) (*QueueSubscriber, error) {
	sub := &QueueSubscriber{Topic: topic}
	// Obtain AWS auth
	// - we pass in null values to get defaults
	// - env variables AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY
	// - on EC2 it will query AWS meta-data service
	auth, err := aws.GetAuth("", "", "", time.Now())
	if err != nil {
		return nil, err
	}
	sqs := sqs.New(auth, aws.USEast)
	sub.Queue, err = sqs.GetQueue(topic)
	if err != nil {
		return nil, err
	}
	return sub, nil
}

func (q *QueueSubscriber) ReceiveMessage(max int, visibility, wait time.Duration) (messages []Message, err error) {
	params := map[string]string{}
	if max != 1 {
		params["MaxNumberOfMessages"] = strconv.Itoa(max)
	}
	if visibility > 0 {
		params["VisibilityTimeout"] = strconv.Itoa(int(visibility.Seconds()))
	}
	if wait > 0 {
		params["WaitTimeSeconds"] = strconv.Itoa(int(wait.Seconds()))
	}
	res, err := q.Queue.ReceiveMessageWithParameters(params)
	if err != nil {
		return nil, err
	}
	for _, msg := range res.Messages {
		messages = append(messages, Message{Id: msg.MessageId, Body: msg.Body, Receipt: msg.ReceiptHandle})
	}
	return
}

func (q *QueueSubscriber) DeleteMessage(message *Message) error {
	msg := &sqs.Message{ReceiptHandle: message.Receipt}
	_, err := q.Queue.DeleteMessage(msg)
	return err
}

type MockSubscriber struct {
	Messages []Message
	Marker   int
	Deleted  []Message
}

func (s *MockSubscriber) ReceiveMessage(max int, visibility, wait time.Duration) (messages []Message, err error) {
	actual := max
	if len(s.Messages) < actual {
		actual = len(s.Messages)
	}
	// IDs are the messages themselves for the mock (we don't require they be unique
	start := s.Marker
	s.Marker += actual
	messages = s.Messages[start:s.Marker]
	return
}

func (s *MockSubscriber) DeleteMessage(message *Message) error {
	s.Deleted = append(s.Deleted, *message)
	return nil
}
